package com.jellyvision.users;

import com.jellyvision.users.model.User;
import com.jellyvision.users.service.UserService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class ServiceTest {

    @Autowired
    UserService userService;

    @Test
    public void testCreateReadDelete() {
        User user = new User("abc-123", "Dave", "Chappelle", "1970-01-01");
        userService.save(user);
        Iterable<User> users = userService.findAll();
        // Assert that the user with the first name "Dave" was created
        Assertions.assertThat(users).extracting(User::getFirst_name).containsOnly("Dave");
        // Assert that the user with the last name "Chappelle" was created
        Assertions.assertThat(users).extracting(User::getLast_name).containsOnly("Chappelle");
        // Assert that the user with the UUID "abc-123" was created
        Assertions.assertThat(users).extracting(User::getUuid).containsOnly("abc-123");
        // Assert that the user with the DOB "1970-01-01" was created
        Assertions.assertThat(users).extracting(User::getDate_of_birth).containsOnly("1970-01-01");

        // Delete everything in the test DB
        userService.deleteAll();

        // Assert that our test user was successfully removed from the database.
        Assertions.assertThat(userService.findAll()).isEmpty();
    }
}
