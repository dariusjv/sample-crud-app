package com.jellyvision.users;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

public class SystemTests {

    @Test
    public void testErrorHandling_UserFetch() {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/user/10101010"; // an id that will never exist
        try {
            restTemplate.getForEntity(url, String.class);
        } catch (HttpClientErrorException e) {
            Assertions.assertEquals(HttpStatus.BAD_REQUEST, e.getStatusCode());
        }
    }
}
