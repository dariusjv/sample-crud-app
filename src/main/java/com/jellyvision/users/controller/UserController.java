package com.jellyvision.users.controller;

import com.jellyvision.users.model.User;
import com.jellyvision.users.service.UserService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.xml.bind.ValidationException;
import java.util.Optional;

@RestController
@Log4j2
public class UserController {
    @Autowired
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    // Create new user (only if name, dob and uuid is present)
    @PostMapping("/user")
    public User create(@RequestBody User user) throws ValidationException {
        if (user.getId() == 0 && user.getFirst_name() != null && user.getLast_name() != null && user.getUuid() != null && user.getDate_of_birth() != null) {
            log.info("New user created: {} {}",  user.getFirst_name(), user.getLast_name());
            return this.userService.save(user);
        } else {
            log.error("User cannot be created");
            throw new ValidationException("User cannot be created");
        }
    }

    // Retrieve all users
    @GetMapping("/user")
    public Iterable<User> read() {
        return this.userService.findAll();
    }

    // Find user by DB AI ID
    @GetMapping("user/{id}")
    public Optional<User> findById(@PathVariable Integer id) {
        return this.userService.findById(id);
    }

    // Find user by UUID
    @GetMapping("user/search")
    public Iterable<User> findByQuery(@RequestParam("uuid") String uuid) {
        return this.userService.findByUuid(uuid);
    }

    // Update user (only if they exist)
    @PutMapping("/user")
    public ResponseEntity<User> update(@RequestBody User user) {
        if (this.userService.findById(user.getId()).isPresent()) {
            log.info("Updating user ID: {}", user.getId());
            return new ResponseEntity(this.userService.save(user), HttpStatus.OK);
        } else {
            log.error("Unable to update user ID: {}", user.getId());
            return new ResponseEntity(user, HttpStatus.BAD_REQUEST);
        }
    }

    // Delete existing user
    @DeleteMapping("/user/{id}")
    public void delete(@PathVariable Integer id) {
        this.userService.deleteById(id);
    }
}
