package com.jellyvision.users.service;

import com.jellyvision.users.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserService extends CrudRepository<User, Integer> {
    Iterable<User> findByUuid(String uuid);
}
