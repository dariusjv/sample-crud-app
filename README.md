# JVPP Onboarding
## CRUD User Service

### Description

- This Spring application will create a RESTful service to add/update/view/delete users.
- The API will ingest a JSON payload containing user information
- The user information will be parsed, validated and stored in MySQL

### Endpoints
- GET /user - Retrieve all users (no pagination)
```
curl --location --request GET 'http://localhost:8080/user'
```
- POST /user - Add new user
```
curl --location --request POST 'http://localhost:8080/user' \
--header 'Content-Type: application/json' \
--data-raw '{
    "first_name": "Steve",
    "last_name": "Jobs",
    "date_of_birth": "1950-01-01",
    "uuid": "123-abc-lol"
}'
```
- PUT /user - Update existing user
```
curl --location --request PUT 'http://localhost:8080/user' \
--header 'Content-Type: application/json' \
--data-raw '{
    "id": 3,
    "uuid": "123-abc-lol",
    "first_name": "Steve",
    "last_name": "Jobs",
    "date_of_birth": "1960-01-01"
}'
```
- DELETE /user/{ID} - Delete existing user by ID
```
curl --location --request DELETE 'http://localhost:8080/user/3'
```
- GET /user/{ID} - Search by UUID
```
curl --location --request GET 'http://localhost:8080/user/1'
```
- GET /user/search?uuid={uuid} - Search by UUID
```
curl --location --request GET 'http://localhost:8080/user/search?uuid=000-00-xyz' \
--header 'Content-Type: application/json' \
--data-raw '{
    "id": 3,
    "uuid": "123-abc-lol",
    "first_name": "Steve",
    "last_name": "Jobs",
    "date_of_birth": "1960-01-01"
}'
```

### Validation
- Users can only be added if first_name/last_name/uuid/date_of_birth are not empty. Example below:
```
curl --location --request POST 'http://localhost:8080/user' \
--header 'Content-Type: application/json' \
--data-raw '{
    "first_name": null,
    "last_name": "Jobs",
    "uuid": "123-abc-lol"
}'
```
- Users can only be updated if they firstly exist, and first_name/last_name/uuid/date_of_birth are not empty
- No date format validation implemented

### Dependencies
- https://mvnrepository.com/artifact/javax.xml.bind/jaxb-api/2.3.0
- MySQL

### Notes
- Created using template from https://start.spring.io
- No pagination for read operations
- https://jira.jellyvision.com/jira/browse/JVPP-746